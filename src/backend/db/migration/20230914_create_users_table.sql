CREATE TABLE users (
    user_id UUID PRIMARY KEY,
    username VARCHAR(255) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL UNIQUE,
    profile_picture VARCHAR(255),
    last_online TIMESTAMP,
    status VARCHAR(255),
);

-- Create a sequence for the UUID primary key
CREATE SEQUENCE users_user_id_seq;

-- Set the default value for the user_id column to use the nextval from the sequence
ALTER TABLE users ALTER COLUMN user_id SET DEFAULT nextval('users_user_id_seq');

-- Create an index on the username column for faster lookups
CREATE INDEX idx_username ON users(username);