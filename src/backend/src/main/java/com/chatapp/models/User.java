package com.chatapp.models;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import com.chatapp.utils.enums.UserStatus;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Entity
@Table(name = "users")
@Data
@NoArgsConstructor 
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private UUID userId;

    @Column(nullable = false, unique = true)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false, unique = true)
    private String email;

    private String profilePicture;

    private LocalDateTime lastOnline;

    @Enumerated(EnumType.STRING)
    private UserStatus status;

    @ManyToMany
    @JoinTable(
            name = "user_contacts",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "contact_id")
    )
    private List<User> contacts;

    @ManyToMany(mappedBy = "members")
    private List<ChatRoom> chats;

    public void setId(UUID id) {
        this.userId = id;
    }
}