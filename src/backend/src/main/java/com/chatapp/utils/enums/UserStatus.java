package com.chatapp.utils.enums;

public enum UserStatus {
    ONLINE, 
    OFFLINE, 
    AWAY,
    BANNED
}