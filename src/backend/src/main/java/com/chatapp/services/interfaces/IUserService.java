package com.chatapp.services.interfaces;

import com.chatapp.models.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface IUserService {

    User saveUser(User user);

    Optional<User> findUserByUsername(String username);

    List<User> findAll();

    Optional<User> findById(UUID id);

    void delete(UUID id);
}


