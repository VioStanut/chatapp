package com.chatapp.services;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chatapp.models.User;
import com.chatapp.repositories.UserRepository;
import com.chatapp.services.interfaces.IUserService;

@Service("userService")
public class UserService implements IUserService {
    
    private final UserRepository userRepository;
    
    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User saveUser(User user) {
        return userRepository.save(user);
    }

    public Optional<User> findUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }    

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public Optional<User> findById(UUID id){
        return userRepository.findById(id);
    }

    public void delete(UUID id){
        userRepository.delete(id);
    }
}